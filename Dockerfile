# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN apt-get update -y
RUN apt-get install apt-utils -y
RUN apt-get install python3-tk python3-opencv -y
COPY . .
ENV DISPLAY :0

CMD [ "python3", "Distortion_removal_tool/src/gui_app.py", "--host=0.0.0.0"]
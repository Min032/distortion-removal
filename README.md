# Distortion removal

A simple desktop app that lets you change perspective on raster images.

# How to start assuming you have docker installed:

```bash
docker build --tag distortion-removal-python .
docker run -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --name distortion-removal-container distortion-removal-python
```

![app](Distortion_removal_tool/images/app.png)